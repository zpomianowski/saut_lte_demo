# SAUT LTE DEMO

Zbiór skryptów i programów mających na celu przedstawić możliwości technologiczne LTE: głównie wysokie szybkoci transmisji danych.

Obecnie cały zestaw jest skonfigurowany _na sztywno_: oznacza to, że najlepiej całość ściągnąć **bezpośrednio na dysk C**.

## Instalacja potrzebnych składników
Instrukcję należy traktować jako ogólne kroki. W zależnoci od komputera mogą się troszkę różnić. W tym wypadku przykład dotycz maszyny _Windows 7 Pro 64bit_.

+ [klient GIT](https://git-scm.com/downloads)
+ [BandWidthMeter](http://www.bandwidth-meter.net/freedown.htm)
 + serial key: `18FC5E4D852398F76134D8E152496FA6BDB5B2575AF4BDA5827B3D4565F3A5CA`
+ [Firefox](https://www.mozilla.org/pl/firefox/new/) v44+
+ [VLC](http://www.videolan.org/vlc/download-windows.html) v2.2.2
+ z wtyczkami
+ [python](http://www.python.org) v2.7.11
+ dodaj do zmiennej środowiskowej PATH: `C:\Python27;C:\Python27\Scripts;C:\Program Files\Git\bin`
+ w konsoli sprawdź typ kompilacji (32/64bit):
```
#!python
import platform
print platform.architecture()
quit()
```
+ ściągamy prekompilowaną paczkę [pyside](http://www.lfd.uci.edu/~gohlke/pythonlibs/#pyside) w wersji 32/64bit (w zależności co nam wyszło w kroku poprzednim)
+ ściągamy prekompilowaną paczkę [pycrypto](http://www.voidspace.org.uk/python/pycrypto-2.6.1/) w wersji 32/64bit (w zależności co nam wyszło wyżej)
+ odpal konsolę, przejdź do lokalizacji paczki _whl_ i:
```
#!bash
pip install <paczka_pyside>.whl
pip install <paczka_pycrypto>.whl
pip install paramiko
```

## Instalujemy naszą paczkę demo
+ wchodzimy bezpośrednio na **dysk C** i w konsoli: `git clone https://saut@bitbucket.org/polsat/saut_lte_demo.git`
 + w przypadku aktualizacji (ale działa w przypadku braku modyfikacji): `git pull origin`
 + prociej jest wywalić i klonować na nowo 
+ odpal konsolę w trybie admina oraz wygeneruj plik do uploadu: `C:\saut_lte_demo\scripts\showThroughputs\UPLOAD\gen_dummy_files`
+ odpal _BandWidthMeter_ i dostosuj wyglšd
+ _Pulpit > Prawy > Personalizuj > Zmień ikony pulpitu > **Odznacz kosz**_
+ może istnieć koniecznoć poprawy cieżek do konkretnych programów w plikach ***.bat** i ***.py**
+ może istnieć koniecznoć dostosowania strony stremowania HD dla konkretnego wywietlacza jeli nie jest HD
 + _C:\saut_lte_demo\scripts_ - ręczna edycja plików ***.html** lub skorzystaj z generatora _streaming-hd-html-generator.py_