import Qt 4.7

Item {
    id: rect
    width: 540
    height: 540
    state: 'INIT'
    Component.onCompleted: rect.state = 'NORMAL'
    Rectangle {
        opacity: .3
    }

    Timer {
        interval: 1500; running: true; repeat: false
        onTriggered: defaultAnim.start()
    }

    MouseArea {
        id: 'buttonMouseArea'
        objectName: 'buttonMouseArea'
        hoverEnabled : true
        anchors.fill: parent
        onClicked: { defaultAnim.stop(); rect.state = 'PRESSED'; buttonMouseArea.enabled = false }
        onEntered: { text1.state = 'IN' }
        onExited: { text1.state = 'OUT' }
    }

    states: [
        State {
            name: 'INIT'
            PropertyChanges { target: black; scale: .01 }
            PropertyChanges { target: text1; scale: .01 }
            PropertyChanges { target: ring; scale: .01 }
        },
        State {
            name: 'NORMAL'
            PropertyChanges { target: black; scale: 1 }
            PropertyChanges { target: text1; scale: .99 }
            PropertyChanges { target: ring; scale: .99 }
        },
        State {
            name: 'PRESSED'
            PropertyChanges { target: black; scale: .0 }
            PropertyChanges { target: text1; scale: .01 }
            PropertyChanges { target: ring; scale: .0 }
        }
    ]

    transitions: [
        Transition {
            from: 'INIT'
            to: 'NORMAL'
            ParallelAnimation {
                 NumberAnimation { target: black; property: 'scale'; duration: 1000; easing.type: Easing.OutBounce }
                 NumberAnimation { target: ring; property: 'scale'; duration: 1100; easing.type: Easing.OutBounce }
                 NumberAnimation { target: text1; property: 'scale'; duration: 1200; easing.type: Easing.OutBounce }
            }
        },
        Transition {
            from: 'NORMAL'
            to: 'PRESSED'
            ParallelAnimation {
                 NumberAnimation { target: black; property: 'scale'; duration: 500; easing.type: Easing.OutQuart }
                 SequentialAnimation {
                    PauseAnimation { duration: 100 }
                    NumberAnimation { target: ring; property: 'scale'; duration: 800; easing.type: Easing.OutExpo }
                 }
                 NumberAnimation { target: text1; property: 'scale'; duration: 400; easing.type: Easing.OutQuart }
            }
        }
    ]

    ParallelAnimation {
        id: defaultAnim
        loops: Animation.Infinite
        running: false
        SequentialAnimation {
            PauseAnimation {
                duration: 200
            }
            PropertyAnimation {
                target: ring
                property: 'scale'
                from: .99
                to: 1
                duration: 1000
                easing.type: Easing.OutBack
            }
            PropertyAnimation {
                target: ring
                property: 'scale'
                from: 1
                to: .99
                duration: 1800
                easing.type: Easing.OutBack
            }
        }
        SequentialAnimation {
            PropertyAnimation {
                target: black
                property: 'scale'
                from: 1
                to: 1.05
                duration: 900
                easing.type: Easing.OutBack
            }
            PropertyAnimation {
                target: black
                property: 'scale'
                from: 1.05
                to: 1
                duration: 2100
                easing.type: Easing.OutBack
            }
        }
    }

    Image {
        id: black
        source: 'tarcza.png'
        smooth: true
    }    

    Image {
        id: ring
        source: 'ring.png'
        smooth: true
    } 

    Image {
    id: text1
        source: 'stop_napis.png'
        smooth: true

        states: [
            State {
                name: 'IN'
                PropertyChanges { target: text1; scale: 1.1 }
            },
            State {
                name: 'OUT'
                PropertyChanges { target: text1; scale: 1 }
            }
        ]

        transitions: [
            Transition {
                from: 'IN'
                to: 'OUT'
                ParallelAnimation {
                     NumberAnimation { target: text1; property: 'scale'; duration: 1000; easing.type: Easing.OutBounce }
                }
            },
            Transition {
                from: 'OUT'
                to: 'IN'
                ParallelAnimation {
                     NumberAnimation { target: text1; property: 'scale'; duration: 1000; easing.type: Easing.OutBounce }
                }
            }
        ]
    }   
}

