import sys
from PySide.QtGui import *
from PySide.QtCore import *
from PySide.QtDeclarative import *
import subprocess

NB = 2

class UplinkTest(QDeclarativeView):
    def __init__(self):
        QDeclarativeView.__init__(self)
        self.initUI()

    def initUI(self):
        self.pids = []
        cmd = "C:\Program Files (x86)\BandwidthMeterPro\BandwidthMeterPro.exe"
        self.pids.append(subprocess.Popen(cmd).pid)

        """ set Window props """
        self.setAttribute(Qt.WA_TranslucentBackground)
        flags = Qt.FramelessWindowHint
        flags |= Qt.WindowStaysOnTopHint
        self.setWindowFlags(flags)
        self.viewport().setAutoFillBackground(False)
        self.setSource('gui.qml')
        #self.center()
        self.move(0, 270)
        self.show()

        root = self.rootObject()
        button = root.findChild(QObject,"buttonMouseArea")
        button.clicked.connect(lambda: self.closeRequest())

        cmd = "UL_LAB_5.172.249.10.bat"
        for i in range(NB):
            startupinfo = subprocess.STARTUPINFO()
            startupinfo.dwFlags |= subprocess._subprocess.STARTF_USESHOWWINDOW
            self.pids.append(subprocess.Popen(cmd, shell=False, startupinfo=startupinfo).pid)
            import time
            time.sleep(1)

    def closeRequest(self):
        """ kill external apps processess """
        for pid in self.pids:
            startupinfo = subprocess.STARTUPINFO()
            startupinfo.dwFlags |= subprocess._subprocess.STARTF_USESHOWWINDOW
            handle = subprocess.Popen("taskkill /F /T /PID %i" % pid , shell=False, startupinfo=startupinfo)
            handle.wait()
            QTimer.singleShot(1000, self, SLOT('close()'))

def main():
    app = QApplication(sys.argv)
    ex = UplinkTest()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
