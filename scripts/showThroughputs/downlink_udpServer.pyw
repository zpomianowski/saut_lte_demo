import sys
from PySide.QtGui import *
from PySide.QtCore import *
from PySide.QtDeclarative import *
import subprocess
import paramiko
import socket

HOST = "5.172.249.10"
DEST = "172.16.223.2"
#DEST = socket.gethostbyname(socket.gethostname())
NB = 6
TP = 60
USER = 'lte'
PASS = 'lte'


class DownlinkTest(QDeclarativeView):

    def __init__(self):
        QDeclarativeView.__init__(self)
        self.initUI()

    def initUI(self):
        """ init external apps """
        self.procs = []
        self.tn = []
        cmd = "C:\Program Files (x86)\BandwidthMeterPro\BandwidthMeterPro.exe"
        # cmd = "C:\Program Files\BandwidthMeterPro\BandwidthMeterPro.exe"
        startupinfo = subprocess.STARTUPINFO()
        startupinfo.dwFlags |= subprocess._subprocess.STARTF_USESHOWWINDOW
        #self.procs.append(subprocess.Popen(cmd, startupinfo=startupinfo))

        cmd = "iperf.exe -s"
        startupinfo = subprocess.STARTUPINFO()
        startupinfo.dwFlags |= subprocess._subprocess.STARTF_USESHOWWINDOW
        self.procs.append(subprocess.Popen(cmd, startupinfo=startupinfo))

        """ set Window props """
        self.setAttribute(Qt.WA_TranslucentBackground)
        flags = Qt.FramelessWindowHint
        flags |= Qt.WindowStaysOnTopHint
        self.setWindowFlags(flags)
        self.viewport().setAutoFillBackground(False)
        self.setSource('gui.qml')
        #self.center()
        self.move(0, 270)
        self.show()

        root = self.rootObject()
        button = root.findChild(QObject, "buttonMouseArea")
        button.clicked.connect(lambda: self.closeRequest())

        """ssh"""
        self.client = paramiko.SSHClient()
        self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.client.connect(HOST, username=USER, password=PASS)
        for i in range(NB):
            stdin, stdout, stderr = self.client.exec_command(
                'iperf -c' + DEST + " -t 99999 -u -b %dM\r &" % TP)

    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        # self.move(qr.topLeft())

    def closeRequest(self):
        """ kill external apps processess """
        self.client.exec_command('pkill -9 iperf')
        for p in self.procs:
            try:
                import os
                import signal
                os.kill(p.pid, signal.SIGTERM)
            except Exception as ex:
                print ex
        QTimer.singleShot(1000, self, SLOT('close()'))


def main():
    app = QApplication(sys.argv)
    ex = DownlinkTest()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
