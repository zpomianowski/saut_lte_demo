import sys
from PySide.QtGui import *
from PySide.QtCore import *
from PySide.QtDeclarative import *
import subprocess
from multiprocessing import Pool
import paramiko
import socket
import re

HOST = "5.172.249.10"
DEST = "100.89.191.34"
DEST = socket.gethostbyname(socket.gethostname())
NB = 5
TP = 65
USER = 'lte'
PASS = 'lte'


class DownlinkTest(QDeclarativeView):

    def __init__(self):
        QDeclarativeView.__init__(self)
        self.initUI()

    def initUI(self):
        """ init external apps """
        self.procs = []
        self.tn = []
        cmd = "C:\Program Files (x86)\BandwidthMeterPro\BandwidthMeterPro.exe"
        # cmd = "C:\Program Files\BandwidthMeterPro\BandwidthMeterPro.exe"
        startupinfo = subprocess.STARTUPINFO()
        startupinfo.dwFlags |= subprocess._subprocess.STARTF_USESHOWWINDOW
        #self.procs.append(subprocess.Popen(cmd, startupinfo=startupinfo))

        """ set Window props """
        self.setAttribute(Qt.WA_TranslucentBackground)
        flags = Qt.FramelessWindowHint
        flags |= Qt.WindowStaysOnTopHint
        self.setWindowFlags(flags)
        self.viewport().setAutoFillBackground(False)
        self.setSource('gui.qml')
        #self.center()
        self.move(0, 270)
        self.show()

        root = self.rootObject()
        button = root.findChild(QObject, "buttonMouseArea")
        button.clicked.connect(lambda: self.closeRequest())

        self.pool = Pool(processes=1)
        result = self.pool.apply_async(ssh_connect_and_run)

    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        # self.move(qr.topLeft())

    def closeRequest(self):
        """ kill external apps processess """
        self.pool.apply_async(ssh_close)
        for p in self.procs:
            try:
                import os
                import signal
                os.kill(p.pid, signal.SIGTERM)
            except Exception as ex:
                print ex
        QTimer.singleShot(10000, self, SLOT('close()'))


def main():
    app = QApplication(sys.argv)
    ex = DownlinkTest()
    sys.exit(app.exec_())


def ssh_connect_and_run():
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    print 'Connecting...'
    client.connect(HOST, username=USER, password=PASS)
    stdin, stdout, stderr = client.exec_command('who am i /')
    print stdout.read()
    #print re.search(r'\(.*?\)', stdout.readlines()).group(1)
    for i in range(NB):
        cmd = 'iperf -c' + DEST + "-p 1200%d -t 99999 -u -b %sM\r &" % (i, TP)
        print cmd
        stdin, stdout, stderr = client.exec_command(cmd)


def ssh_close():
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(HOST, username=USER, password=PASS)
    client.exec_command('pkill -9 iperf')

if __name__ == '__main__':
    main()
