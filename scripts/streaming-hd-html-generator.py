# -*- coding: utf-8 -*-
from win32api import GetSystemMetrics

# można zmieniac
CLIPS1 = [
    'http://5.172.249.10:49152/content/media/object_id/211/res_id/0',
    'http://5.172.249.10:49152/content/media/object_id/232/res_id/0',
    'http://5.172.249.10:49152/content/media/object_id/214/res_id/0',
    'http://5.172.249.10:49152/content/media/object_id/223/res_id/0',
    'http://5.172.249.10:49152/content/media/object_id/217/res_id/0',
    'http://5.172.249.10:49152/content/media/object_id/235/res_id/0',
    'http://5.172.249.10:49152/content/media/object_id/229/res_id/0',
    'http://5.172.249.10:49152/content/media/object_id/220/res_id/0',
    'http://5.172.249.10:49152/content/media/object_id/226/res_id/0',
    'http://5.172.249.10:49152/content/media/object_id/210/res_id/0'
]

CLIPS2 = [
    'http://5.172.249.2:49152/content/media/object_id/54/res_id/0',
    'http://5.172.249.2:49152/content/media/object_id/60/res_id/0',
    'http://5.172.249.2:49152/content/media/object_id/58/res_id/0',
    'http://5.172.249.2:49152/content/media/object_id/57/res_id/0',
    'http://5.172.249.2:49152/content/media/object_id/59/res_id/0'
]

CLIPS3 = [
    'http://5.172.249.10:49152/content/media/object_id/147/res_id/0',
    'http://5.172.249.2:49152/content/media/object_id/50/res_id/0',
    'http://5.172.249.10:49152/content/media/object_id/148/res_id/0',
    'http://5.172.249.2:49152/content/media/object_id/50/res_id/0',
    'http://5.172.249.10:49152/content/media/object_id/149/res_id/0',
    'http://5.172.249.2:49152/content/media/object_id/50/res_id/0',
    'http://5.172.249.10:49152/content/media/object_id/150/res_id/0',
    'http://5.172.249.2:49152/content/media/object_id/50/res_id/0',
    'http://5.172.249.10:49152/content/media/object_id/151/res_id/0',
    'http://5.172.249.2:49152/content/media/object_id/50/res_id/0',
    'http://5.172.249.10:49152/content/media/object_id/152/res_id/0',
    'http://5.172.249.2:49152/content/media/object_id/50/res_id/0',
    'http://5.172.249.10:49152/content/media/object_id/153/res_id/0',
    'http://5.172.249.2:49152/content/media/object_id/50/res_id/0',
    'http://5.172.249.2:49152/content/media/object_id/50/res_id/0',
    'http://5.172.249.2:49152/content/media/object_id/50/res_id/0'
]
CLIPS = CLIPS1

TITLE = 'LTE live full HD video streaming'
MUTE = True
TOOLBAR = False
GRID_XNB = 3
GRID_YNB = 3

# poniższych lepiej nie zmieniać
screen_w = GetSystemMetrics(0)
screen_h = GetSystemMetrics(1)
grid_w = screen_w / GRID_XNB
grid_h = screen_h / GRID_YNB

html = """
<html>
<head><title>%(title)s</title></head>
<style>
html { overflow: -moz-scrollbars-none; }
</style>
<body bgcolor="000000" topmargin=0 leftmargin=0 rightmargin=0 overflow="hidden">
<table cellpadding="0" cellspacing="0" border="0">
""" % dict(title=TITLE)

clip_nb = 0
for x in range(GRID_XNB):
    html += "<tr>"
    for y in range(GRID_YNB):
        html += """
        <td>
            <embed type="application/x-vlc-plugin"
            id="video%(nb)d"
            name="video%(nb)d"
            toolbar = %(toolbar)s
            controls = %(toolbar)s
            mute = %(mute)s
            autoplay="yes" loop="yes" width="%(w)d" height="%(h)d"
            target="%(target)s"/>
        </td>
        """ % dict(nb=clip_nb, toolbar=TOOLBAR,
                   mute=MUTE, w=grid_w, h=grid_h, target=CLIPS[clip_nb])
        clip_nb += 1
        clip_nb = clip_nb%len(CLIPS)
    html += "</tr>"

f = open('streaming_lte.html', 'w')
f.write(html)
f.close()


